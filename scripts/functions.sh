#!/bin/bash

name=function3
names=Jonny


func1() 
{
    echo "Welcome to the shell scripting"
}

function func2
{
    echo "This is function 2"
}

func3()
{
    echo "This is ${name}"
}

func4()
{
    local var1=LEEI
    echo "Argument ${var1}"
}

func5()
{
    echo "Welcome ${names}"
    echo "Argument 1 ${var1}"
}

func1;
# Or
func1
func2;
func3;
func4;
func5;

echo "checking local var1 access: ${var1}"