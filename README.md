# Linux shell and scripting tutorial #

## Course overview ##
- Detail introduction of shell
- Primary purpose of shell scripts
- Practical part 1. First script, Shebang!, Variable scope, commandline arguments, functions
- Practical part 2. Synchronous and asyncronous commands execution, Shell spacial variables, return code and exit status, looping and condition constructs

## What is shell and Shell script?
- Shell is command interpreter and a programming language, At its base as hell is simple micro processor that executes commands
- Shell script is set of instruction / program designed to run by the shell

## Types of shells ##
- sh - Bourne shell. developmed by Strphen Bourne at bell labs, it was a replacement for the Thompson shell, whose executable file had the same 'sh'
- Bash - Bourne shell again. Was in 1989 developed for the GNU project and incorporates features from the Bourne shell, csh, and ksh, It is meant ot be POSIX compliant.
- csh, ksh, zsh and more ..
- Shell config files ~/.nashrc, ~/.bash_profile, ~/.profile

```
> which sh
/bin/sh
> which bash
/bin/bash

> /bin/bash --version

> /bin/bash --help

> vi ~/.bashrc

> exit

> vi ~/.bash_profile

> echo $PATH
```

## Scripting practical - part 1 ##
- First script
- Command line argguments
- functions
- variables and variables cope


## Feartures of shell script ##
- Script can be invoke as commands by using their filename
- May be used interactively or non-interactively
- Allows both synchronous and asynchronous execution of commands
- Provides a set of built-in commands
- Provides flow control constructs, quotation facilities, and functions.
- Type less variables
- Provides local and global variable scope
- Script do not require compilation before execution
- No limits on string lengths when inerpreting shell scripts

Script practical part 2
- Synchronous % asynchronous command  execution
- shell special variable 
- $@, $#, $*, $$,$?
- Return code and exit status
- Looping and condition construct: if, case, for

## Redirections ##
- Redirections output
- Appending redirected output
- Redirecting standard output and error
- Redirect input
- Pipes

```
ls -l > output

ls -l p2.sh > output

ls -l p2.sh >> output
```

